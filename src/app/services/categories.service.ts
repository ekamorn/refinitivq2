import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(
    private http: HttpClient
  ) { }

  // get categories
  getCategories() {
    const apiUrl = 'https://api.publicapis.org/categories';
    const response = this.http.get(apiUrl);

    return response;
  }
}
