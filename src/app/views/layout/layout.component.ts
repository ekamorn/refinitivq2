import { Component, OnInit } from '@angular/core';
import { CategoriesService } from './../../services/categories.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  categories: string[] = [];
  cates: string[] = [];
  filter: string = "";

  constructor(
    private categoriesService: CategoriesService
  ) { }

  ngOnInit(): void {

    this.categoriesService.getCategories()
    .subscribe((data: any) => {
      this.cates = data;
      this.renderTable(data);
    });
  }

  renderTable(data:any) {

    let cates: string[] = data;
    if (this.filter != "") {
      let newData:string[] = [];
      let lowerFilter = this.filter.toLowerCase();
      cates.forEach(element => {

        let lowerIndex = element.toLowerCase();
        if (lowerIndex.indexOf(lowerFilter) > -1) {
          newData.push(element);
        }
      });
      this.categories = newData;
    }
    else {
      this.categories = data;
    }
  }

  onChangeFilter() {

    setTimeout(() => {
      this.renderTable(this.cates);
    }, 1000);
  }
}
